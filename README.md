Есть дизайн десктопа, моушн. 
Необходимо сверстать футер. Сделать десктоп и адаптивы. 
Анимации согласно <a href="https://combo.agency/test_gsap.mp4">моушену</a> на gsap, адаптивы согласно твоему вкусу.
Не делаем: обработку ошибок и градиент. Но нужно сделать состояния ховер и фокус для инпутов. 
Сборщики scss можно, всякие бутстрапы и джиквери не нужно использовать👌

Футер:
<img src="https://combo.agency/footer.png" alt="footer">
Форма: 
<img src="https://combo.agency/form.jpg" alt="form">
<a href="https://www.figma.com/file/zvpkWYOw8ElKCNMjwW11KB/test_front?node-id=0%3A1">Ссылка на макет</a>
<br>
<video width="400" height="300" controls="controls">
   <source src="https://combo.agency/test_gsap.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
   <a href="https://combo.agency/test_gsap.mp4">Ссылка на моушн</a>
</video>

